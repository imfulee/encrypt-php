# 加密方法

1. 先去下載 submodule 

```bash
git submodule update --init
```

2. 記得去修改 `encrypt-php.js` 裏面 `tonyenc.php` 的路徑，應該是在 `tonyenc/` 中，但是我自己爲了方便管理才放到 PHP 的路徑下

```javascript
const phpScriptPath = "C:/xampp/php/tonyenc.php";
```

3. 去修改 `php.ini` 加 extension，重啓 php

```
extension=tonyenc.so
```

4. 使用方法
