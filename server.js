import { createRequire } from "module";
const require = createRequire(import.meta.url);

const express = require("express");
const path = require("path");
const multer = require("multer");
const __dirname = path.resolve();

import encrypter from "./encrypt-php.js";

const app = express();
const port = process.env.PORT || 8000;

const fileStorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});
const upload = multer({ storage: fileStorageEngine });

// sendFile will go here
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "/index.html"));
});

// post to server
app.post("/upload-zip", upload.single("file"), (req, res) => {
  console.log(req.body.filename);
  console.dir(req.file);

  encrypter("./uploads/file.zip").then((done) => {
    if(done) res.sendFile(path.join(__dirname, "/output/output.zip"));
  })
});

app.listen(port);
console.log("Server started at http://localhost:" + port);
