import { createRequire } from "module";
const require = createRequire(import.meta.url);

const fs = require("fs");
const util = require("util");
const unzipper = require("unzipper");
const archiver = require("archiver");

import ora from "ora";

const zipPath = "./src/file.zip";

function execute(zipPath, zippedFilename) {
  const unzippedPath = `./unzipped/${zippedFilename}`;
  // to change the path
  const phpScriptPath = "C:/xampp/php/tonyenc.php";
  
  return new Promise((resolve, reject) => {
    if (fs.existsSync(zipPath)) {
      var done = false;
      const unzipLog = ora("解壓縮文件檔案").start();
      fs.createReadStream(zipPath)
        .pipe(unzipper.Extract({ path: "unzipped/" }))
        .promise()
        .then(() => unzipLog.succeed())
        .then(() => encryptFile(phpScriptPath, unzippedPath))
        .then(() => {
          const zipLog = ora("壓縮文件").start();
          zipDirectory(unzippedPath, "./output/output.zip").then(
            () => {
              zipLog.succeed();
              done = true;
            }
          );
        });
    }
  });
}

async function encryptFile(phpScriptPath, unzippedPath) {
  /**
   * Encrypt PHP file
   */
  const encryptLog = ora("加密 PHP 文件").start();
  const exec = util.promisify(require("child_process").exec);
  const argsString = phpScriptPath + " " + unzippedPath;
  const execReturn = await exec("php " + argsString);
  if (execReturn.stderr === "") encryptLog.succeed();
  return true;
}

async function zipDirectory(sourceDir, outPath) {
  const archive = archiver("zip", { zlib: { level: 9 } });
  const stream = fs.createWriteStream(outPath);

  return new Promise((resolve, reject) => {
    archive
      .directory(sourceDir, false)
      .on("error", (err) => reject(err))
      .pipe(stream);

    stream.on("close", () => resolve());
    archive.finalize();
  });
}

// execute(zipPath)
// .then((done) => console.log(done));

export default execute;
